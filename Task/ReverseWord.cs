﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Task
{
    public class ReverseWord
    {

        private const char currentSeparator = ' ';

        public string ReverseWordWithoutSymbol(string text)
        {
            if (String.IsNullOrEmpty(text))
            {
                return String.Empty;
            }

            Dictionary<int, char> repositorySymbol = new Dictionary<int, char>();
            List<char> textWithoutSymbol = new List<char>();

            for (int i = 0; i < text.Length; i++)
            {
                if (Char.IsLetter(text[i]) 
                    || text[i] == currentSeparator)
                {
                    textWithoutSymbol.Add(text[i]);
                }
                else
                {
                    repositorySymbol.Add(i, text[i]);
                }
            }

            string changedText = String.Join("", textWithoutSymbol);

            changedText = WordsReverse(changedText);

            foreach (var item in repositorySymbol)
            {
                changedText = changedText.Insert(item.Key, item.Value.ToString());
            }

            return changedText;
        }



        private string WordsReverse(string text)
        {
            var textLength = text.Length;
            var retValue = new char[textLength];
            var wordStartIndex = 0;

            while (wordStartIndex < textLength)
            {
                var separatorIndex = IndexOf(text, wordStartIndex);
                if (separatorIndex == -1)
                {
                    separatorIndex = text.Length;
                }
                else
                {
                    retValue[separatorIndex] = currentSeparator;
                }

                var currentIndex = wordStartIndex;
                for (var i = separatorIndex - 1; i >= wordStartIndex; i--)
                {
                    retValue[currentIndex] = text[i];
                    currentIndex++;
                }

                wordStartIndex = separatorIndex + 1;
            }

            return new string(retValue);
        }

        private int IndexOf(string text, int startIndex)
        {
            for (var i = startIndex; i < text.Length; i++)
            {
                if (text[i] == currentSeparator)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
