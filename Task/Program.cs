﻿using System;

namespace Task
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                ReverseWord separator = new ReverseWord();
                Console.WriteLine("Enter text");
                string text = Console.ReadLine();

                Console.WriteLine(separator.ReverseWordWithoutSymbol(text));
            }
        }
    }
}
