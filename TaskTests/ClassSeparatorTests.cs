﻿
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Task.Tests
{

    public class ClassSeparatorTests
    {

        [Test]
        [TestCase("abc", "cba")]
        [TestCase("1fgh! ffgh@", "1hgf! hgff@")]
        [TestCase("fhse efhu@ jsi'e wad", "eshf uhfe@ eis'j daw")]
        [TestCase("fsefn@$#$mfsl# efes#$^%&", "lsfmn@$#$fesf# sefe#$^%&")]
        [TestCase("Аіы@", "ыіА@")]
        [TestCase("", "")]
        [TestCase(" ", " ")]
        [TestCase("fjksj afjei", "jskjf iejfa")]
        [TestCase(null, "")]
        public void ReverseWordWithoutSymbolTest(string source, string res)
        {
            //Arange

            ReverseWord separator = new ReverseWord();

            //Act

            //Assert
            Assert.AreEqual(separator.ReverseWordWithoutSymbol(source), res);

        }

        
    }

}
